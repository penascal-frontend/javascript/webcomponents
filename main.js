(async () => {
    const character_count = 50

    const parseCharacter = (data) => `
        <my-card
            title="${data.name}"
            image="${data.image}"
            alt="Avatar de ${data.name}"
            href="https://rickandmortyapi.com/"
            text="Explora el API"
        >
            <ul slot="description">
                <li>Origen: ${data?.origin?.name  ?? 'Indefinido'}</li>
                <li>Especie: ${data?.species ?? 'Indefinido'}</li>
                <li>Género: ${data?.gender ?? 'Indefinido'}</li>
                <li>Estado: ${data?.status ?? 'Indefinido'}</li>
            </ul>
        </my-card>`

    const getData = async (index) =>
        await (await fetch(`https://rickandmortyapi.com/api/character/${index}`)).json()

    document.getElementById('app').innerHTML = (await Promise.all(
        Array.from(Array(character_count).keys()).map(
            async index => parseCharacter(await getData(index+1))
        )
    )).join('')
    
})()