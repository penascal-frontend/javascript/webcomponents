export class Card extends HTMLElement {
  #elements

  constructor() {
    super()

    this.title = 'Card title'
    this.image = './no-image.png'
    this.alt = 'image alternative text'
    this.description = 'Card description'
    this.href = '#'
    this.text = 'Read more'

    const template = document.createElement('template')
    template.innerHTML = /*html*/ `
        <img>
        <header><slot name="title"></slot></header>
        <slot name="description"><p></p></slot>
        <footer><a></a></footer>
        <style>
          * {
              margin: 0;
              padding: 0;
              box-sizing: border-box;
          }
          :host {
            --width: 240px;
            border: 1px solid black;
            border-radius: 0.5rem;
            gap: 0;
            display: flex;
            flex-direction: column;
            max-width: 240px;
            overflow: hidden;
          }
          header {
            padding: 0.5rem;
            text-align: center;
            font-size: 1.5rem;
          }


          img {
            width: 100%;
            display: block;
            border-radius: 0.5rem 0.5rem 0 0;
          }
          
          p {
            padding: 0.5rem;
            flex: 1;
          }

          slot[name="title"] {
            color: green;
          }

          footer {
            padding: 0.5rem;
            text-align: right;
            font-size: 0.8rem;
            text-transform: uppercase;
          }
          footer a, footer a:visited {
            text-decoration: none;
            cursor: pointer;
            color: blue
          }
        </style>
      `
    const root = this.attachShadow({ mode: "closed" })
    root.appendChild(document.importNode(template.content, true))
    this.#elements = {
      image: root.children[0],
      header: root.children[1],
      desc: root.children[2].children[0],
      link: root.children[3].children[0],
    }
  }

  static get observedAttributes() {
    return ['title', 'image', 'alt', 'description', 'href', 'text']
  }

  attributeChangedCallback(property, oldValue, newValue) {
    if (oldValue === newValue) return
    this[property] = newValue
    this.#render()
  }

  connectedCallback() {
    this.#render()
  }

  #render() {
    const { header, image, desc, link } = this.#elements
    image.src = this.image
    image.alt = this.alt
    header.innerText = this.title
    desc.innerText = this.description
    link.href = this.href
    link.innerText = this.text
  }
}
